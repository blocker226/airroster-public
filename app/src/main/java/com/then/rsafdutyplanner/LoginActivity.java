package com.then.rsafdutyplanner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {
    public static final String LOGIN_TYPE = "LoginType";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    // TODO: Combine loginMen and loginOfficer into a single login() method, when data is read from
    //  the server
    protected void loginMen(View v){
        Intent intent = new Intent(this, DashboardActivity.class);
        String loginType = "Men";
        intent.putExtra(LOGIN_TYPE, loginType);
        startActivity(intent);
    }

    protected void loginOfficer(View v){
        Intent intent = new Intent(this, DashboardActivity.class);
        String loginType = "Officer";
        intent.putExtra(LOGIN_TYPE, loginType);
        startActivity(intent);
    }
}
